import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;
import java.util.InputMismatchException;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class homework_cycle {
    public static int[][] randomArray(int firstArrayLength, int secondArrayLength) {
        int array[][] = new int[firstArrayLength][secondArrayLength];
        Random rand = new Random();
        for (int i = 0; i < firstArrayLength; i++) {
            for (int j = 0; j < secondArrayLength; j++) {
                array[i][j] = rand.nextInt(99);
            }
        }
        return array;
    }

    public static int findSmallerNumber(int array[][]) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < array.length; ++i) {
            for (int j = 0; j < array[0].length; ++j) {
                if (min > array[i][j]) {
                    min = array[i][j];
                }
            }
        }
        return min;
    }

    public static int arraySize() {
        Scanner scanner = new Scanner(System.in);
        int arrayLength = 0;
        boolean error = false;
        do {
            error = false;
            try {
                System.out.println("Введите число");
                arrayLength = scanner.nextInt();
            } catch (InputMismatchException exception) {
                System.out.println("Введенное занчние не является числом, ведите число");
                error = true;
                scanner.nextLine();
            }
        } while (error);
        return arrayLength;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Высота массива:");
        int firstArrayLength = arraySize();
        System.out.println("Длина масстива:");
        int secondArrayLength = arraySize();
        int array[][] = new int[firstArrayLength][secondArrayLength];
        array = randomArray(firstArrayLength, secondArrayLength);
        System.out.println("Ваш массив:");
        for (int[] ints : array) {
            System.out.println(Arrays.toString(ints));
        }
        System.out.println(findSmallerNumber(array) + " - Минимальное число массива");
    }
}


